import {apiRequest} from "../helpers/apiRequest";
import {api_methods} from "../constants/api_method";

export const login = (data) => {
   return apiRequest('/api/login', api_methods.post, data);
};
