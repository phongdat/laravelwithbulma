export const setAuthorization = (token) => {
    axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
};

export const initialize = (store, router) => {
    router.beforeEach((to, from, next) => {
        const requiresAuth = to.matched.some(record => record.meta.isAuth);
        const currentUser = store.state.currentUser;
        if(requiresAuth && !currentUser) {
            next('/login');
        } else if(to.path === '/login' && currentUser) {
            next('/');
        } else {
            next();
        }
        axios.interceptors.response.use(null, (error) => {
            if (error.response.status === 401) {
                store.commit('logout');
                if(to.path !== '/login') {
                    router.push('/login');
                }
            }
            return Promise.reject(error);
        });

        if (store.getters.currentUser) {
            setAuthorization(store.getters.currentUser.access_token);
        }
    });
};

export const getLocalUser = () => {
    const userStr = localStorage.getItem("xxx-user");
    if(!userStr) return null;
    return JSON.parse(userStr);
};
