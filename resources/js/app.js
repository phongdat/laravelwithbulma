require('./bootstrap');
import Vue from 'vue';
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import routes from './routes';
import dataStore from './store';
import {initialize} from "./helpers/general";
import RootComponent from './components/RootComponent';

Vue.use(VueRouter);
Vue.use(Vuex);

const router = new VueRouter({
   //mode: 'history',
   routes
});
const store = new Vuex.Store(dataStore);
initialize(store, router);
const app = new Vue({
    el: '#app',
    components: {RootComponent},
    template: '<RootComponent />',
    router,
    store
});
