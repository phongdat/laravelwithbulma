import HomeComponent from './components/HomeComponent';
import LoginComponent from './components/auth/LoginComponent';
import RegisterComponent from './components/auth/RegisterComponent';

const routes = [
    {path: '/', component: HomeComponent, meta: {isAuth: true}},
    {path: '/login', component: LoginComponent, meta: {isAuth: false}},
    {path: '/register', component: RegisterComponent, meta: {isAuth: false}},
];

export default routes;
