import {getLocalUser} from "../helpers/general";
const user = getLocalUser();

export default {
    state: {
        welcome: 'Welcome bulma css framework',
        currentUser: user,
        loading: false,
        auth_error: null,
        errors: []
    },
    getters: {
        welcome(state) {
            return state.welcome;
        },
        auth_error(state) {
            return state.auth_error;
        },
        currentUser(state) {
            return state.currentUser;
        },
        errors(state) {
            return state.errors;
        },
        loading(state) {
            return state.loading;
        }
    },
    mutations: {
        login: state => {
            state.auth_error = null;
            state.loading = true;
            state.errors = [];
        },
        loginFailed(state, payload) {
            state.loading = false;
            let status = payload.status;
            switch (status) {
                case 422:
                    state.errors = payload.data.errors;
                    break;
                case 401:
                    state.auth_error = 'unauthenticated';
                    break;
                default:
                    state.auth_error = 'UNKNOWN_ERROR';
                    break;
            }
        },
        loginSuccess(state, payload) {
            console.log(payload);
            state.auth_error = null;
            state.loading = false;
            state.currentUser = Object.assign({}, payload.data);
            localStorage.setItem("xxx-user", JSON.stringify(state.currentUser));
        },
        logout(state) {
            localStorage.removeItem("xxx-user");
            state.currentUser = null;
        }
    },
    actions: {
        login(context) {
            context.commit('login')
        }
    }
}
