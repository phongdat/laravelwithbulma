export const api_methods = {
    post: 'post',
    get: 'get',
    put: 'put',
    delete: 'delete'
};
