<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class OauthClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('oauth_clients')->delete();
        $datetime = Carbon::now();
        $clients = [
            [
                'id' => 1,
                'user_id' => null,
                'name'    => 'Laravel Personal Access Client',
                'secret' => 'GVnIcDEsvomPGyPNJ5oMKb0kDKLGD4QwgC6A5u88',
                'redirect' => 'http://localhost',
                'personal_access_client' => 1,
                'password_client' => 0,
                'revoked' => 0,
                'created_at' => $datetime,
                'updated_at' => $datetime
            ],
            [
                'id' => 2,
                'user_id' => null,
                'name'    => 'Laravel Password Grant Client',
                'secret' => 'sMIqSBxH1dbgpyOGebFWCxxYcfMgnZAl0qtSLzdj',
                'redirect' => 'http://localhost',
                'personal_access_client' => 0,
                'password_client' => 1,
                'revoked' => 0,
                'created_at' => $datetime,
                'updated_at' => $datetime
            ],
        ];
        DB::table('oauth_clients')->insert($clients);

    }
}
