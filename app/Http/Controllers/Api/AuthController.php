<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends ApiBaseController
{
    public function login(Request $request)
    {
        $request->validate([
           'email' => 'required|email',
           'password' => 'required'
        ]);
        $credentials = request(['email', 'password']);
        $checkAuth = Auth::attempt($credentials);
        if(!$checkAuth) return $this->returnUnAuthorizedError('Unauthorized');
        $data = self::responsePassport($request);
        $user = auth()->user();
        $data['user_name'] = $user->name;
        $data['user_email'] = $user->email;
        return $this->returnSuccess($data);
    }

    protected static function responsePassport($request)
    {
        $data = [
            'grant_type'    => 'password',
            'client_id'     => config('services.passport.password_client_id'),
            'client_secret' => config('services.passport.password_client_secret'),
            'username'      => $request->email,
            'password'      => $request->password,
            'scope'         => '*'
        ];
        $request = Request::create('/oauth/token', 'POST', $data);
        $response = app()->handle($request);
        $content = json_decode($response->getContent(), true);
        return $content;
    }
}
