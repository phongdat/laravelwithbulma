<?php
namespace App\Traits;

interface StandardCode {
    const UNKNOWN_ERROR = 0000;
    /* ------- Request ------------------*/
    const INVALID_PARAMS = 1001;
    /* ------- RESIDENT ------------------*/
    const RESIDENT_NOT_EXITS = 3001;
    /* ------- MAIL ------------------*/
    const SEND_MAIL_FAILED = 5000;
    /* ------- OAUTH ------------------*/
    const LOGIN_OAUTH_FAILED = 6001;
    /* ------- FILE ------------------*/
    const INVALID_FILE = 7001;
    /*------- PASSPORT_AUTH ----------*/
    const ERROR_CODE_UNAUTHORIZED = 401;
    const HTTP_ERROR_CODE_UNAUTHORIZED = 401;

}

trait ApiResponse
{
    protected function returnSuccess($data = null)
    {
        $return = ['success' => 1];
        if(!empty($data)){
            $return['data'] = $data;
        }
        return response()->json($return);
    }

    /**
     * @param $error_code
     * @param null $mess
     * @param int $return_code
     * @return \Illuminate\Http\JsonResponse
     * @internal param array $data
     */
    protected function returnError($error_code, $mess = null, $return_code = 400){
        if(empty($mess)){
            $mess = [__('messages.error_mess.server_error')];
        } else if ( !is_array($mess)){
            $mess = [$mess];
        }

        return response()->json([
            'success' => 0,
            'error_code' => $error_code,
            'msg' => $mess,
        ], $return_code);
    }

    /**
     * Unauthorized error
     *
     * @param null $mess
     * @return \Illuminate\Http\JsonResponse
     */
    protected function returnUnAuthorizedError($mess = null)
    {
        if(empty($mess)){
            $mess = [__('messages.error_mess.unauthorized')];
        } elseif (!is_array($mess)){
            $mess = [$mess];
        }

        return $this->returnError(StandardCode::ERROR_CODE_UNAUTHORIZED, $mess, StandardCode::HTTP_ERROR_CODE_UNAUTHORIZED);
    }

    /**
     * Bad request
     *
     * @param null $mess
     * @return \Illuminate\Http\JsonResponse
     */
    protected function returnBadRequestError($mess = null)
    {
        if(empty($mess)){
            $mess = [__('messages.error_mess.bad_request')];
        } elseif (!is_array($mess)){
            $mess = [$mess];
        }

        return $this->returnError(StandardCode::INVALID_PARAMS, $mess, 400);
    }

    /**
     * @param $field_message_map  ex. ['field_1' => ['msg1'],'field_2' => ['msg2'],]
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     * @internal param null $mess
     * @internal param int $return_code
     * @internal param array $data
     */
    protected function returnValidationError($field_message_map)
    {
        throw \Illuminate\Validation\ValidationException::withMessages($field_message_map);
    }

    protected function failed($code = StandardCode::UNKNOWN_ERROR, $data = null, $mess = null, $statusCode = 400){
        $return['success'] = 0;
        $return['code'] = $code;
        if(!empty($mess)) {
            $return['mess'] = $mess;
        }
        if(!empty($data)) {
            $return['data'] = $data;
        }
        return response()->json($return, $statusCode);
    }
}
